//
//  Users.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class Users: NSObject{
    var email : String = String()
    var username : String = String()
    var fullname : String = String()
    var password : String = String()
    var bio : String = String()
    var phone : String = String()
    var picture : String = String()
}
