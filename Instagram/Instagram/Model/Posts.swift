//
//  Posts.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class Posts: NSObject{
    var username : String = String()
    var image : String = String()
    var caption : String = String()
}
