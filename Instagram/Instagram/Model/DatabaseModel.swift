//
//  UsersModel.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

let sharedInstance = DatabaseModel()

class DatabaseModel: NSObject {
    
    var database: FMDatabase? = nil
    
    class func getInstance() -> DatabaseModel
    {
        if(sharedInstance.database == nil)
        {
            sharedInstance.database = FMDatabase(path: Util.getPath("instagramDB.sqlite"))
        }
        return sharedInstance
    }
    
    func addUsersData(userInfo: Users) -> Bool {
        sharedInstance.database!.open()
        let isInserted = sharedInstance.database!.executeUpdate("INSERT INTO users (username, fullname, email, password) VALUES (?, ?, ?, ?)", withArgumentsInArray: [userInfo.username, userInfo.fullname, userInfo.email, userInfo.password])
        sharedInstance.database!.close()
        return isInserted
    }
    
    func updateUsersData(userInfo: Users) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE users SET fullname = ?, bio = ?, phone = ?, password = ? WHERE username = ?", withArgumentsInArray: [userInfo.fullname, userInfo.bio, userInfo.phone, userInfo.password, userInfo.username])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func updateUsersPhoto(photo: String, userInfo: Users) -> Bool {
        sharedInstance.database!.open()
        let isUpdated = sharedInstance.database!.executeUpdate("UPDATE users SET picture = ? WHERE username = ?", withArgumentsInArray: [photo, userInfo.username])
        sharedInstance.database!.close()
        return isUpdated
    }
    
    func getUserData(usernameSearch:String) -> Users {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM users where username = ?", withArgumentsInArray: [usernameSearch])
        let userOn : Users = Users()
        if (resultSet != nil) {
            while resultSet.next() {
                userOn.email = resultSet.stringForColumn("email")
                userOn.username = resultSet.stringForColumn("username")
                userOn.password = resultSet.stringForColumn("password")
                userOn.fullname = resultSet.stringForColumn("fullname")
                if(resultSet.stringForColumn("bio") == nil){
                    userOn.bio = ""
                } else {
                    userOn.bio = resultSet.stringForColumn("bio")
                }
                if(resultSet.stringForColumn("phone") == nil){
                    userOn.phone = ""
                } else {
                    userOn.phone = resultSet.stringForColumn("phone")
                }
                if(resultSet.stringForColumn("picture") == nil){
                    userOn.picture = ""
                } else {
                    userOn.picture = resultSet.stringForColumn("picture")
                }
            }
        }
        sharedInstance.database!.close()
        return userOn
    }

    func getDataOrang(usernameSearch:String) -> Users {
    sharedInstance.database!.open()
    let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM users where username = ?", withArgumentsInArray: [usernameSearch])
    let userOn : Users = Users()
    if (resultSet != nil) {
    while resultSet.next() {
    userOn.username = resultSet.stringForColumn("username")
    userOn.fullname = resultSet.stringForColumn("fullname")
    if(resultSet.stringForColumn("bio") == nil){
    userOn.bio = ""
    } else {
    userOn.bio = resultSet.stringForColumn("bio")
    }
    if(resultSet.stringForColumn("phone") == nil){
    userOn.phone = ""
    } else {
    userOn.phone = resultSet.stringForColumn("phone")
    }
    if(resultSet.stringForColumn("picture") == nil){
    userOn.picture = ""
    } else {
    userOn.picture = resultSet.stringForColumn("picture")
    }
    }
    }
    sharedInstance.database!.close()
    return userOn
    }
    
    func getAllFollowing(username:String) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM follow where username = ?", withArgumentsInArray: [username])
        let allFollowing : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let following : Follow = Follow()
                following.username = username
                following.follow = resultSet.stringForColumn("follow")
                allFollowing.addObject(following)
            }
        }
        sharedInstance.database!.close()
        return allFollowing
    }
    
    func getAllFollower(username:String) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM follow where follow = ?", withArgumentsInArray: [username])
        let allFollowing : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let following : Follow = Follow()
                following.follow = username
                following.username = resultSet.stringForColumn("username")
                
                allFollowing.addObject(following)
            }
        }
        sharedInstance.database!.close()
        return allFollowing
    }
    
    
    func searchUserData(usernameSearch:String) -> Users {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM users where username = ?", withArgumentsInArray: [usernameSearch])
        let userOn : Users = Users()
        if (resultSet != nil) {
            while resultSet.next() {
                userOn.email = resultSet.stringForColumn("email")
                userOn.username = resultSet.stringForColumn("username")
                userOn.fullname = resultSet.stringForColumn("fullname")
                if(resultSet.stringForColumn("bio") == nil){
                    userOn.bio = ""
                } else {
                    userOn.bio = resultSet.stringForColumn("bio")
                }
                if(resultSet.stringForColumn("phone") == nil){
                    userOn.phone = ""
                } else {
                    userOn.phone = resultSet.stringForColumn("phone")                }
            }
        }
        sharedInstance.database!.close()
        return userOn
    }
    
    func addFollow(foll:Follow) -> Bool {
        sharedInstance.database!.open()
        let isAdd = sharedInstance.database!.executeUpdate("INSERT INTO follow (username, follow) VALUES (?, ?)", withArgumentsInArray: [foll.username, foll.follow])
        sharedInstance.database!.close()
        return isAdd
    }
    
    func unFollow(foll:Follow) -> Bool {
        sharedInstance.database!.open()
        let isDelete = sharedInstance.database!.executeUpdate("DELETE FROM follow where username = ? and follow = ?", withArgumentsInArray: [foll.username, foll.follow])
        sharedInstance.database!.close()
        return isDelete
    }

    
    func isFollow(foll:Follow) -> Bool {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM follow where username = ? and follow = ?", withArgumentsInArray: [foll.username, foll.follow])
        var check : Bool = false
        if (resultSet != nil) {
            while resultSet.next() {
                check = true
            }
        }
        sharedInstance.database!.close()
        return check
    }
    
    func addPost(post:Posts) -> Bool {
        sharedInstance.database!.open()
        let isAdd = sharedInstance.database!.executeUpdate("INSERT INTO posts (username, image, caption) VALUES (?, ?, ?)", withArgumentsInArray: [post.username, post.image, post.caption])
        sharedInstance.database!.close()
        return isAdd
    }
    
    
    func deletePost(post:Posts) -> Bool {
        sharedInstance.database!.open()
        let isDelete = sharedInstance.database!.executeUpdate("DELETE FROM posts where username = ? and image = ?", withArgumentsInArray: [post.username, post.image])
        sharedInstance.database!.close()
        return isDelete
    }

    
    func getUserPost(username:String) -> [Posts] {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM posts where username = ? order by rowid desc", withArgumentsInArray: [username])
        var allPost : [Posts] = [Posts]()
        var i : Int = 0
        if (resultSet != nil) {
            while resultSet.next() {
                let post : Posts = Posts()
                post.username = username
                post.image = resultSet.stringForColumn("image")
                post.caption = resultSet.stringForColumn("caption")
                
                allPost.insert(post, atIndex: i)
                i++;
            }
        }
        sharedInstance.database!.close()
        return allPost
    }
    
    func getTimelinePost(username:String) -> [Posts] {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM posts where username in (SELECT follow from follow where username = ?) or username = ? order by rowid desc", withArgumentsInArray: [username, username])
        var allPost : [Posts] = [Posts]()
        var i : Int = 0
        if (resultSet != nil) {
            while resultSet.next() {
                let post : Posts = Posts()
                post.username = resultSet.stringForColumn("username")
                post.image = resultSet.stringForColumn("image")
                post.caption = resultSet.stringForColumn("caption")
                
                allPost.insert(post, atIndex: i)
                i++;
            }
        }
        sharedInstance.database!.close()
        return allPost
    }

    func getAllPost() -> [Posts] {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM posts order by rowid", withArgumentsInArray: [])
        var allPost : [Posts] = [Posts]()
        var i : Int = 0
        if (resultSet != nil) {
            while resultSet.next() {
                let post : Posts = Posts()
                post.username = resultSet.stringForColumn("username")
                post.image = resultSet.stringForColumn("image")
                post.caption = resultSet.stringForColumn("caption")
                
                allPost.insert(post, atIndex: i)
                i++;
            }
        }
        sharedInstance.database!.close()
        return allPost
    }
    
    func getAllLikes(image:String) -> NSMutableArray {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM likes where postImage = ?", withArgumentsInArray: [image])
        let allLikes : NSMutableArray = NSMutableArray()
        if (resultSet != nil) {
            while resultSet.next() {
                let likes : Likes = Likes()
                likes.image = image
                likes.username = resultSet.stringForColumn("username")
                allLikes.addObject(likes)
            }
        }
        sharedInstance.database!.close()
        return allLikes
    }

    func isLike(like:Likes) -> Bool {
        sharedInstance.database!.open()
        let resultSet: FMResultSet! = sharedInstance.database!.executeQuery("SELECT * FROM likes where  username = ? and postImage = ?", withArgumentsInArray: [like.username, like.image])
        var check : Bool = false
        if (resultSet != nil) {
            while resultSet.next() {
                check = true
            }
        }
        sharedInstance.database!.close()
        return check
    }
    
    func addLike(like:Likes) -> Bool {
        sharedInstance.database!.open()
        let isAdd = sharedInstance.database!.executeUpdate("INSERT INTO likes (username, postImage) VALUES (?, ?)", withArgumentsInArray: [like.username, like.image])
        sharedInstance.database!.close()
        return isAdd
    }
    
    func disLike(like:Likes) -> Bool {
        sharedInstance.database!.open()
        let isDelete = sharedInstance.database!.executeUpdate("DELETE FROM likes where username = ? and postImage = ?", withArgumentsInArray: [like.username, like.image])
        sharedInstance.database!.close()
        return isDelete
    }
    
}
