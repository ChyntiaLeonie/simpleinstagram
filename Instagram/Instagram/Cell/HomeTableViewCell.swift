//
//  HomeTableViewCell.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit
class HomeTableViewCell: UITableViewCell {
    
    var imageName: String! = String()
    
    @IBOutlet weak var imageUser: UIImageView!
    
    @IBOutlet weak var postUsername: UILabel!
   
    @IBOutlet weak var postImage: UIImageView!
    
    @IBOutlet weak var caption: UILabel!
    
    @IBOutlet weak var like: UIButton!
    
    @IBOutlet weak var banyakLike: UILabel!
    
    var likes : Likes! = Likes()
    
    @IBAction func buttonLike(sender: AnyObject) {
        let image = sender.currentImage
        if(image == UIImage(named: "notification")){
            DatabaseModel.getInstance().addLike(likes)
            like.setImage(UIImage(named: "love"), forState: UIControlState.Normal)
        } else {
            DatabaseModel.getInstance().disLike(likes)
            like.setImage(UIImage(named: "notification"), forState: UIControlState.Normal)
        }
        var allLikes = DatabaseModel.getInstance().getAllLikes(imageName)
        banyakLike.text = String(allLikes.count)+" like(s)"
    }
}