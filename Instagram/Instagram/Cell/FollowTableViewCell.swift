//
//  FollowTableViewCell.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/20/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit
class FollowTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageUser: UIImageView!

    @IBOutlet weak var username: UILabel!
    }