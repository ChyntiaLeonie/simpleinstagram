//
//  ExploreCollectionViewCell.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/5/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageExplore: UIImageView!
}
