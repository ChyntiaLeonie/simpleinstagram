//
//  ProfileSearchCollectionViewCell.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class ProfileSearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel : UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
}