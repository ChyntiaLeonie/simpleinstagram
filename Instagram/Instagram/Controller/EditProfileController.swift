//
//  EditProfileController.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class EditProfileController: UIViewController {
    
    var userOn : Users!
    
    @IBOutlet weak var name: UITextField!
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var bio: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var phoneNumber: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    @IBAction func cancelButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func checkButton(sender: AnyObject) {
        var user : Users! = Users()
        user.username = userOn.username
        user.fullname = name.text!
        user.bio = bio.text!
        user.email = email.text!
        user.phone = phoneNumber.text!
        user.password = password.text!
        
        print(user.username, user.fullname, user.bio, user.email, user.phone, user.password)
        
        DatabaseModel.getInstance().updateUsersData(user!)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        var username = userOn.username
    
        userOn = DatabaseModel.getInstance().getUserData(username)
        self.name.text = userOn.fullname
        self.username.text = userOn.username
        self.bio.text = userOn.bio ?? ""
        self.email.text = userOn.email
        self.phoneNumber.text = userOn.phone
        self.password.text = userOn.password
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}