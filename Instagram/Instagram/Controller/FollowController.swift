//
//  FollowController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/20/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class FollowController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userOn : Users!
    var userOpen : Users!
    var user : NSMutableArray = NSMutableArray()
    var lihatProfile : Users! = Users()
    var usernameClick : Follow! = Follow()
    var pilih : String!
    
    // Don't forget to enter this in IB also
    let cellReuseIdentifier = "cellFollow"
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        print(userOn.username, userOpen.username, user.count)
    }
    
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.user.count
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:FollowTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier) as! FollowTableViewCell
        let follow:Follow = user.objectAtIndex(indexPath.row) as! Follow
        if(userOpen.username == follow.follow){
            cell.username.text = follow.username
            var u = DatabaseModel.getInstance().getUserData(follow.username)
            if( u.picture == ""){
                cell.imageUser.image = UIImage(named: "photoProfile")
            } else {
                var path: String = getPath(userOn.picture as String)
                let imageFromPath = UIImage(contentsOfFile: path)!
                cell.imageUser.image = imageFromPath
            }
        } else {
            cell.username.text = follow.follow
            var u = DatabaseModel.getInstance().getUserData(follow.follow)
            if( u.picture == ""){
                cell.imageUser.image = UIImage(named: "photoProfile")
            } else {
                var path: String = getPath(userOn.picture as String)
                let imageFromPath = UIImage(contentsOfFile: path)!
                cell.imageUser.image = imageFromPath
            }


        }
        return cell
    }
    
    func getPath(fileName: String) -> String {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        print(fileURL.path)
        return fileURL.path!
    }
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    var globalVar : Int = 0
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueProfile" {
            let selectedIndex = self.tableView.indexPathForCell(sender as! UITableViewCell)
            globalVar = selectedIndex!.row
            
            let dest = segue.destinationViewController as! ProfileSearchController
            usernameClick = user.objectAtIndex(globalVar) as! Follow
            dest.userOn = userOn
            if(pilih == "Following"){
                lihatProfile = DatabaseModel.getInstance().getUserData(usernameClick.follow)
            } else {
                lihatProfile = DatabaseModel.getInstance().getUserData(usernameClick.username)
            }
            if(userOn.username == lihatProfile.username){
                dest.ket = "Edit Profile"
                dest.userSearch = userOn
                
            } else {
                dest.userSearch = lihatProfile
                var follow : Follow = Follow()
                follow.username = userOn.username
                follow.follow = lihatProfile.username
                if(DatabaseModel.getInstance().isFollow(follow)){
                    dest.ket = "Following"
                } else {
                    dest.ket = "Follow"
                }
            }
        }
    }
    
}