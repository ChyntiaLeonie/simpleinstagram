//
//  ProfileSearchController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/19/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//
import UIKit

class ProfileSearchController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate{
    
    @IBOutlet weak var username: UINavigationItem!
    @IBOutlet weak var imageProfile: UIImageView!
    var userOn : Users!
    var userSearch : Users!
    var ket : String!
    
    @IBOutlet weak var buttonFollow: UIButton!
    
    @IBOutlet weak var buttonEdit: UIButton!
    
    @IBOutlet weak var banyakPost: UILabel!
    @IBAction func followOnClick(sender: AnyObject) {
        print(buttonFollow.titleLabel?.text)
            }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String!, sender: AnyObject!) -> Bool {
        if identifier == "seguePop"{
            if(buttonFollow.titleLabel?.text == "Edit Profile"){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("EditProfPicController") as!EditProfPicController
                return true
            } else {
                buttonEdit.enabled = false
            }
        }
        if identifier == "segueEdit1" {
            if(buttonFollow.titleLabel?.text == "Edit Profile"){
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("EditProfileController") as!EditProfileController
                return true
            } else if(buttonFollow.titleLabel?.text == "Follow"){
                var follow : Follow = Follow()
                follow.username = userOn.username
                follow.follow = userSearch.username
                DatabaseModel.getInstance().addFollow(follow)
                buttonFollow.setTitle("Following", forState: .Normal)
                
                allFollowing = DatabaseModel.getInstance().getAllFollowing(userSearch.username)
                following.text = String(allFollowing.count)
                allFollower = DatabaseModel.getInstance().getAllFollower(userSearch.username)
                follower.text = String(allFollower.count)
                return false
            } else {
                var follow : Follow = Follow()
                follow.username = userOn.username
                follow.follow = userSearch.username
                DatabaseModel.getInstance().unFollow(follow)
                buttonFollow.setTitle("Follow", forState: .Normal)
                
                allFollowing = DatabaseModel.getInstance().getAllFollowing(userSearch.username)
                following.text = String(allFollowing.count)
                allFollower = DatabaseModel.getInstance().getAllFollower(userSearch.username)
                follower.text = String(allFollower.count)
                return false
            }
        }
        return true
    }
    
    override func prepareForSegue(segue:
        UIStoryboardSegue, sender: AnyObject?) {
            if segue.identifier == "seguePop" {
                let popoverViewController = segue.destinationViewController
                popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
                popoverViewController.popoverPresentationController!.delegate = self
                let dest = segue.destinationViewController as! EditProfPicController
                dest.userOn = userOn
            }
        if segue.identifier == "segueEdit1" {
            let dest = segue.destinationViewController as! EditProfileController
            dest.userOn = userOn
        }
        if segue.identifier == "segueFollower" {
            let followView = segue.destinationViewController as! FollowController
            followView.userOn = userOn
            followView.userOpen = userSearch
            followView.user = allFollower
            followView.pilih = "Follower"
        }
        if segue.identifier == "segueFollowing" {
            let followView = segue.destinationViewController as! FollowController
            followView.userOn = userOn
            followView.userOpen = userSearch
            followView.user = allFollowing
            followView.pilih = "Following"
        }

    }
    
    var allFollower : NSMutableArray = NSMutableArray()
    var allFollowing : NSMutableArray = NSMutableArray()
    
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var follower: UILabel!
    
    var allUserPosts : [Posts] = [Posts]()

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        print(userOn.username)
        
        username.title = userSearch.username
        userSearch = DatabaseModel.getInstance().getDataOrang(username.title!)
        buttonFollow.setTitle(ket, forState: .Normal)
        allFollowing = DatabaseModel.getInstance().getAllFollowing(userSearch.username)
        following.text = String(allFollowing.count)
        allFollower = DatabaseModel.getInstance().getAllFollower(userSearch.username)
        follower.text = String(allFollower.count)
        print(allUserPosts.count)
        if(userSearch.username != userOn.username){
            buttonEdit.enabled = false
        }
        if(userSearch.picture == ""){
            imageProfile.image = UIImage(named: "photoProfile")
        } else {
            var path: String = getPath(userOn.picture as String)
            let imageFromPath = UIImage(contentsOfFile: path)!
            imageProfile.image = imageFromPath
        }
        banyakPost.text = String(allUserPosts.count)
    }
    
    
    func getPath(fileName: String) -> String {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        return fileURL.path!
    }

    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    
    
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.allUserPosts.count)
        return self.allUserPosts.count
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        print("asd")
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ProfileSearchCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        var path: String = getPath(allUserPosts[indexPath.item].image as String)
        let imageFromPath = UIImage(contentsOfFile: path)!
        cell.imageView.image = imageFromPath
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        let postDC = self.storyboard?.instantiateViewControllerWithIdentifier("postDetailController") as! PostDetailController
        
        postDC.image = imageProfile.image
        postDC.username = userSearch.username
        
        var path: String = getPath(allUserPosts[indexPath.row].image as String)
        let imageFromPath = UIImage(contentsOfFile: path)!
        postDC.post = imageFromPath
        postDC.capt = allUserPosts[indexPath.row].caption
        postDC.user = userOn
        postDC.userOn = userOn
        self.navigationController?.pushViewController(postDC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allUserPosts = DatabaseModel.getInstance().getUserPost(userSearch.username)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}