//
//  PostDetailController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class PostDetailController: UIViewController, UIPopoverPresentationControllerDelegate {
    
    var image : UIImage!
    var post : UIImage!
    var username : String!
    var capt : String!
    var posts : Posts! = Posts()
    var user : Users!
    var userOn : Users!
    
    @IBOutlet weak var buttonMore: UIButton!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var usernamePost: UILabel!
    @IBOutlet weak var imagePost: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        caption.text = capt
        imageProfile.image = image
        usernamePost.text = username
        imagePost.image = post
        if(username != userOn.username){
            buttonMore.enabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverSetting" {
            let popoverViewController = segue.destinationViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
            popoverViewController.popoverPresentationController!.delegate = self
            
            let sharePost = segue.destinationViewController as! PostDetailSettingController
            sharePost.image = post
            sharePost.caption = capt
            sharePost.post = posts
            sharePost.user = user
        }
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
}