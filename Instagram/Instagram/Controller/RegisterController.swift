//
//  RegisterController.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class RegisterController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var fullname: UITextField!
    
    @IBOutlet weak var username: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    var usersData : Users!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let backgroundImage = UIImageView(frame: UIScreen.mainScreen().bounds)
        backgroundImage.image = UIImage(named: "background")
        self.view.insertSubview(backgroundImage, atIndex: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    func isValidUsername(testStr:String) -> Bool {
        let usernameRegEx = "^(?=.{8,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$"
        
        let usernameTest = NSPredicate(format:"SELF MATCHES %@", usernameRegEx)
        return usernameTest.evaluateWithObject(testStr)
    }
    
    @IBAction func btnRegisterClicked(sender: AnyObject)
    {
        if(email.text == "")
        {
            let alert = UIAlertController(title: "", message: "Please enter an email", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(fullname.text == "")
        {
            let alert = UIAlertController(title: "", message: "Please enter a fullname", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(username.text == "")
        {
            let alert = UIAlertController(title: "", message: "Please enter a username", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(password.text == "")
        {
            let alert = UIAlertController(title: "", message: "Please enter a password", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(!isValidEmail(email.text!)){
            let alert = UIAlertController(title: "", message: "Please enter a valid email", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else if(!isValidUsername(username.text!)){
            let alert = UIAlertController(title: "", message: "Please enter a valid username", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else {
            let usersData: Users = Users()
            usersData.fullname = fullname.text!
            usersData.email = email.text!
            usersData.username = username.text!
            usersData.password = password.text!
            let isInserted = DatabaseModel.getInstance().addUsersData(usersData)
            if isInserted {
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarController") as!UITabBarController
            } else {
                let alert = UIAlertController(title: "", message: "Error", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueRegister" {
            let usersData: Users = Users()
            usersData.fullname = fullname.text!
            usersData.email = email.text!
            usersData.username = username.text!
            usersData.password = password.text!
            let tab = segue.destinationViewController as! UITabBarController
            let navHome = tab.viewControllers![0] as! UINavigationController
            let navExplore = tab.viewControllers![1] as! UINavigationController
            let navUpload = tab.viewControllers![2] as! UINavigationController
            let navProfile = tab.viewControllers![4] as! UINavigationController
            let home : HomeController = navHome.topViewController as! HomeController
            let explore : ExploreController = navExplore.topViewController as! ExploreController
            let upload : UploadController = navUpload.topViewController as! UploadController
            let profile : ProfileController = navProfile.topViewController as! ProfileController
            
            home.userOn = usersData
            explore.userOn = usersData
            upload.userOn = usersData
            profile.userOn = usersData
        }
    }
}