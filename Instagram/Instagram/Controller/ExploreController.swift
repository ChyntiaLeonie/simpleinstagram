//
//  ExploreController.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class ExploreController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate{
    
    var userOn : Users!
    var allPost : [Posts] = [Posts]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print(userOn.username)
        allPost = DatabaseModel.getInstance().getAllPost()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var user : Users!
    
    @IBOutlet weak var searchNama: UITextField!
    @IBAction func searchButton(sender: AnyObject) {
    }
    
    override func shouldPerformSegueWithIdentifier(identifier: String!, sender: AnyObject!) -> Bool {
        user = DatabaseModel.getInstance().searchUserData(searchNama.text!)
        if identifier == "searchSegue" {
            if(user.username==""){
                print("masuk sini")
                let alert = UIAlertController(title: "", message: "Invalid username", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                return false
            } else {
                print("masuk ke yang else")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewControllerWithIdentifier("ProfileSearchController") as!ProfileSearchController
                return true
            }
        }
        return true
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "searchSegue" {
            let dest = segue.destinationViewController as! ProfileSearchController
            dest.userOn = userOn
            user = DatabaseModel.getInstance().getUserData(searchNama.text!)
            if(userOn.username == searchNama.text){
                dest.ket = "Edit Profile"
                dest.userSearch = userOn
            } else {
                dest.userSearch = user
                print("cobain"+userOn.username)
                var follow : Follow = Follow()
                follow.username = userOn.username
                follow.follow = searchNama.text!
                if(DatabaseModel.getInstance().isFollow(follow)){
                    dest.ket = "Following"
                } else {
                    dest.ket = "Follow"
                }
            }
        }
    }
    
    //Collection View
    let reuseIdentifier = "cellExplore" // also enter this string as the cell identifier in the storyboard
    
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allPost.count
    }
    
    func getPath(fileName: String) -> String {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        print(fileURL.path)
        return fileURL.path!
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ExploreCollectionViewCell
        
        var path: String = getPath(self.allPost[indexPath.item].image as String)
        let imageFromPath = UIImage(contentsOfFile: path)!

        cell.imageExplore.image = imageFromPath
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // handle tap events
        let postDC = self.storyboard?.instantiateViewControllerWithIdentifier("postDetailController") as! PostDetailController
        
        var userFoto = DatabaseModel.getInstance().getUserData(allPost[indexPath.row].username)
        if(userFoto.picture == ""){
            postDC.image = UIImage(named: "photoProfile")
        } else {
            var path: String = getPath(userFoto.picture as String)
            let imageFromPath = UIImage(contentsOfFile: path)!
            postDC.image = imageFromPath
        }
        
        
        var path2: String = getPath(self.allPost[indexPath.row].image as String)
        let imageFromPath2 = UIImage(contentsOfFile: path2)!
        postDC.username = userFoto.username
        postDC.post = imageFromPath2
        postDC.capt = allPost[indexPath.row].caption
        postDC.user = userOn
        postDC.userOn = userOn
        self.navigationController?.pushViewController(postDC, animated: true)
    }
    
}