//
//  ProfileController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//


import UIKit

class ProfileController: UIViewController, UICollectionViewDelegate, UIPopoverPresentationControllerDelegate {
    
    
    @IBOutlet weak var username: UINavigationItem!
    
    var userOn : Users!
    
    @IBOutlet weak var banyakPost: UILabel!
    @IBOutlet weak var following: UILabel!
    @IBOutlet weak var follower: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    var post : Posts! = Posts()
    
    var allFollower : NSMutableArray = NSMutableArray()
    var allFollowing : NSMutableArray = NSMutableArray()
    var allUserPosts : [Posts] = [Posts]()
    
    
    @IBAction func followerButton(sender: AnyObject) {
        
        
    }
    
    @IBAction func followingButton(sender: AnyObject) {
    }
    
    
    @IBAction func logoutButton(sender: AnyObject) {
    }
    
    @IBOutlet weak var buttonFollower: UIButton!
    
    @IBAction func buttonEdit2(sender: AnyObject) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        username.title = userOn.username
        
        allUserPosts = DatabaseModel.getInstance().getUserPost(userOn.username)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        var userTemp = userOn.username
        userOn = DatabaseModel.getInstance().getUserData(userTemp)
        
        allFollowing = DatabaseModel.getInstance().getAllFollowing(userOn.username)
        following.text = String(allFollowing.count)
        allFollower = DatabaseModel.getInstance().getAllFollower(userOn.username)
        follower.text = String(allFollower.count)
        if(userOn.picture == ""){
            profileImage.image = UIImage(named: "photoProfile")
        } else {
            var path: String = getPath(userOn.picture as String)
            let imageFromPath = UIImage(contentsOfFile: path)!
            profileImage.image = imageFromPath
        }
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("imageTapped:"))
        profileImage.userInteractionEnabled = true
        profileImage.addGestureRecognizer(tapGestureRecognizer)
        banyakPost.text = String(allUserPosts.count)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //Collection View
    let reuseIdentifier = "cellProfile" // also enter this string as the cell identifier in the storyboard
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allUserPosts.count
    }
    
    
    func getPath(fileName: String) -> String {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        return fileURL.path!
    }
    
    // make a cell for each cell index path
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ProfileCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        var path: String = getPath(allUserPosts[indexPath.item].image as String)
        let imageFromPath = UIImage(contentsOfFile: path)!
        cell.imageView.image = imageFromPath
        cell.backgroundColor = UIColor.whiteColor()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let postDC = self.storyboard?.instantiateViewControllerWithIdentifier("postDetailController") as! PostDetailController
        
        postDC.image = profileImage.image
        postDC.username = userOn.username
        
        var path: String = getPath(allUserPosts[indexPath.row].image as String)
        let imageFromPath = UIImage(contentsOfFile: path)!
        postDC.post = imageFromPath
        postDC.capt = allUserPosts[indexPath.row].caption
        post.image = allUserPosts[indexPath.row].image
        post.username = allUserPosts[indexPath.row].username
        postDC.posts = post
        postDC.user = userOn
        postDC.userOn = userOn
        
        self.navigationController?.pushViewController(postDC, animated: true)
    }
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.None
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "popoverProfilePicture" {
            
            let popoverViewController = segue.destinationViewController
            popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
            popoverViewController.popoverPresentationController!.delegate = self
            
            let editPicture = segue.destinationViewController as! EditProfPicController
            editPicture.userOn = userOn
        }

        if segue.identifier == "segueEdit2" {
            let dest = segue.destinationViewController as! EditProfileController
            dest.userOn = userOn
        }
        if segue.identifier == "segueFollower" {
            let followView = segue.destinationViewController as! FollowController
            followView.userOn = userOn
            followView.userOpen = userOn
            followView.user = allFollower
            followView.pilih = "Follower"
        }
        if segue.identifier == "segueFollowing" {
            let followView = segue.destinationViewController as! FollowController
            followView.userOn = userOn
            followView.userOpen = userOn
            followView.user = allFollowing
            followView.pilih = "Following"
        }
        
    }
    
    
}