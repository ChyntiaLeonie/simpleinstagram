//
//  WriteCaptionController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit
import Social

class WriteCaptionController: UIViewController{
    
    var image : UIImage!
    var user : Users! = Users()
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var imageCaption: UITextField!
    
    @IBAction func uploadButton(sender: AnyObject) {
        
    }
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    @IBOutlet weak var caption: UITextField!
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueWriteCaption" {
            let imageName = NSUUID().UUIDString
            let imagePath = getDocumentsDirectory().stringByAppendingPathComponent(imageName)
            
            if let jpegData = UIImageJPEGRepresentation(imageView.image!, 80) {
                jpegData.writeToFile(imagePath, atomically: true)
            }
            
            var post : Posts! = Posts()
            post.username = user.username
            post.image = imageName
            post.caption = caption.text! ?? ""
            
            var ok : Bool
            ok = DatabaseModel.getInstance().addPost(post)
            

            let tab = segue.destinationViewController as! UITabBarController
            let navHome = tab.viewControllers![0] as! UINavigationController
            let navExplore = tab.viewControllers![1] as! UINavigationController
            let navUpload = tab.viewControllers![2] as! UINavigationController
            let navProfile = tab.viewControllers![4] as! UINavigationController
            let home : HomeController = navHome.topViewController as! HomeController
            let explore : ExploreController = navExplore.topViewController as! ExploreController
            let upload : UploadController = navUpload.topViewController as! UploadController
            let profile : ProfileController = navProfile.topViewController as! ProfileController
            
            home.userOn = user
            explore.userOn = user
            upload.userOn = user
            profile.userOn = user
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

