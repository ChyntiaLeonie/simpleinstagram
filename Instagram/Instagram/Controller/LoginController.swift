//
//  LoginController.swift
//  Instagram
//
//  Created by LabImac-01 on 6/28/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class LoginController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    var user : Users!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backgroundImage = UIImageView(frame: UIScreen.mainScreen().bounds)
        backgroundImage.image = UIImage(named: "background")
        self.view.insertSubview(backgroundImage, atIndex: 0)

        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnLoginClicked(sender: AnyObject){
        user = DatabaseModel.getInstance().getUserData(username.text!)
        if(username.text == ""){
                let alert = UIAlertController(title: "", message: "Please enter username", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            } else if(password.text == ""){
                let alert = UIAlertController(title: "", message: "Please enter password", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            } else {
                if(password.text == user.password){
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewControllerWithIdentifier("TabBarController") as!UITabBarController
                    
                } else {
                    let alert = UIAlertController(title: "", message: "Invalid username/password", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "LoginSegue" {
            let tab = segue.destinationViewController as! UITabBarController
            let navHome = tab.viewControllers![0] as! UINavigationController
            let navExplore = tab.viewControllers![1] as! UINavigationController
            let navUpload = tab.viewControllers![2] as! UINavigationController
            let navProfile = tab.viewControllers![4] as! UINavigationController
            let home : HomeController = navHome.topViewController as! HomeController
            let explore : ExploreController = navExplore.topViewController as! ExploreController
            let upload : UploadController = navUpload.topViewController as! UploadController
            let profile : ProfileController = navProfile.topViewController as! ProfileController
            
            home.userOn = user
            explore.userOn = user
            upload.userOn = user
            profile.userOn = user
        }
    }
}
