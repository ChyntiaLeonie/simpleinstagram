//
//  UploadController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/13/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class UploadController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var userOn : Users!
    
    @IBOutlet var imageView: UIImageView!
    let imagePicker = UIImagePickerController()
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .ScaleAspectFit
            imageView.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController((imagePicker), animated: true, completion: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueNextUpload" {
            let next = segue.destinationViewController as! WriteCaptionController
            next.image = imageView.image
            next.user = userOn
        }
    }

    
}

