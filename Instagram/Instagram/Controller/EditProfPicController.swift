//
//  EditProfPicController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/27/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class EditProfPicController: UIViewController{
    
    var userOn : Users! = Users()
    
    @IBAction func buttonRemove(sender: AnyObject) {
        DatabaseModel.getInstance().updateUsersPhoto("", userInfo: userOn)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func buttonEdit(sender: AnyObject) {
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("EPPC", userOn.username)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueNextUploadProfPic" {
            print("masuk segue")
            let next = segue.destinationViewController as! UploadProfPicController
            next.userOn = userOn
        }
    }
    
}
