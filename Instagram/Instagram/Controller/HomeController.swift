//
//  HomeController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/12/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class HomeController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var userOn : Users!
        // These strings will be the data for the table view cells
    var allTimelinePost : [Posts] = [Posts]()
  
    // Don't forget to enter this in IB also
    let cellReuseIdentifier = "cellHome"
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allTimelinePost = DatabaseModel.getInstance().getTimelinePost(userOn.username)
        
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        var userTemp = userOn.username
        userOn = DatabaseModel.getInstance().getUserData(userTemp)
        allTimelinePost = DatabaseModel.getInstance().getTimelinePost(userOn.username)
        
    }
    
    // number of rows in table view
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allTimelinePost.count
    }
    
    
    func getPath(fileName: String) -> String {
        
        let documentsURL = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)[0]
        let fileURL = documentsURL.URLByAppendingPathComponent(fileName)
        return fileURL.path!
    }
    
    // create a cell for each table view row
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:HomeTableViewCell = self.tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier) as! HomeTableViewCell
        
        var userPost = DatabaseModel.getInstance().getDataOrang(self.allTimelinePost[indexPath.row].username)
        if(userPost.picture == ""){
            cell.imageUser.image = UIImage(named: "photoProfile")
        } else {
            var path: String = getPath(userPost.picture as String)
            let imageFromPath = UIImage(contentsOfFile: path)!
            cell.imageUser.image = imageFromPath
        }
        cell.likes.image = self.allTimelinePost[indexPath.row].image
        cell.likes.username = userOn.username
        if(DatabaseModel.getInstance().isLike(cell.likes)){
            cell.like.setImage(UIImage(named: "love"), forState: UIControlState.Normal)
        } else {
            cell.like.setImage(UIImage(named: "notification"), forState: UIControlState.Normal)
        }
        var allLikes = DatabaseModel.getInstance().getAllLikes(allTimelinePost[indexPath.row].image)
        cell.banyakLike.text = String(allLikes.count)+" like(s)"
        cell.postUsername.text = self.allTimelinePost[indexPath.row].username
        var path2: String = getPath(allTimelinePost[indexPath.row].image as String)
        let image2FromPath = UIImage(contentsOfFile: path2)!
        cell.imageName = allTimelinePost[indexPath.row].image
        cell.postImage.image = image2FromPath
        cell.caption.text = self.allTimelinePost[indexPath.row].caption
        
        return cell
    }
    
    
    // method to run when table view cell is tapped
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    }
}