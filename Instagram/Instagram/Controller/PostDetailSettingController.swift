//
//  PostDetailSettingController.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/14/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit
import Social

class PostDetailSettingController: UIViewController{
    
    var image : UIImage!
    var caption : String!
    var post : Posts! = Posts()
    var user : Users! = Users()
    
    @IBAction func shareOnFBClicked(sender: AnyObject) {
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook) {
            
            let facebookShare:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            
            let facebook = caption
            facebookShare.addImage(image)
            facebookShare.setInitialText(facebook)
            
            
            self.presentViewController(facebookShare, animated: true, completion: nil)
            
        }
        else {
            let alert = UIAlertController(title: "Facebook Login Required", message: "Go to Settings -> facebook & login", preferredStyle: UIAlertControllerStyle.Alert)
            
            let okAction = UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler: nil)
            
            alert.addAction(okAction)
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    @IBAction func deletePost(sender: AnyObject) {
        print(DatabaseModel.getInstance().deletePost(post))
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "deleteSegue" {
            let tab = segue.destinationViewController as! UITabBarController
            let navHome = tab.viewControllers![0] as! UINavigationController
            let navExplore = tab.viewControllers![1] as! UINavigationController
            let navUpload = tab.viewControllers![2] as! UINavigationController
            let navProfile = tab.viewControllers![4] as! UINavigationController
            let home : HomeController = navHome.topViewController as! HomeController
            let explore : ExploreController = navExplore.topViewController as! ExploreController
            let upload : UploadController = navUpload.topViewController as! UploadController
            let profile : ProfileController = navProfile.topViewController as! ProfileController
            
            home.userOn = user
            explore.userOn = user
            upload.userOn = user
            profile.userOn = user
        }
    }

    
}
