//
//  UploadPhotoProfile.swift
//  Instagram
//
//  Created by Chyntia Leonie Andreas on 7/27/16.
//  Copyright © 2016 LabImac-01. All rights reserved.
//

import UIKit

class UploadProfPicController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var userOn : Users! = Users()
    
    @IBOutlet var imageView: UIImageView!
    let imagePicker = UIImagePickerController()
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.contentMode = .ScaleAspectFit
            imageView.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func getDocumentsDirectory() -> NSString {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }

    @IBAction func uploadButton(sender: AnyObject) {
        
        
        print("ini username: ",userOn.username," haha")
        let imageName = NSUUID().UUIDString
        let imagePath = getDocumentsDirectory().stringByAppendingPathComponent(imageName)
        
        if let jpegData = UIImageJPEGRepresentation(imageView.image!, 80) {
            jpegData.writeToFile(imagePath, atomically: true)
        }
        
        DatabaseModel.getInstance().updateUsersPhoto(imageName, userInfo: userOn)
        

        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    @IBAction func choosePhoto(sender: AnyObject) {
        
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .PhotoLibrary
        
        presentViewController((imagePicker), animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

